@extends('backend/layout')
@section('content')
<section class="content-header">
  <h1>Company</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Comapny</li>
    <li class="active">Add Page</li>
  </ol>
</section>
<!-- Main content -->
<section id="main-content" class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">{{$company->page_title}}</h3>
          <button class="btn btn-primary pull-right">Back</button>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
          {{ Form::open(array('route' => $company->form_action, 'method' => 'POST', 'files' => true, 'id' => 'user-form')) }}
          {{ Form::hidden('id', $company->id, array('id' => 'id')) }}
          <div id="form-display-name" class="form-group ">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
              <span class="label label-danger label-required">Required</span>
              <strong class="field-title">Name</strong>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
              {{ Form::text('name', $company->name, array('placeholder' => '', 'class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
            </div>
          </div>
          <div id="form-display-mail" class="form-group ">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
              <span class="label label-danger label-required">Required</span>
              <strong class="field-title">Email</strong>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
              {{ Form::email('email', $company->email, array('placeholder' => '', 'class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
            </div>
          </div>
          <div id="form-display-mail" class="form-group ">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
              <span class="label label-danger label-required">Required</span>
              <strong class="field-title">Postcode</strong>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-content">
              {{ Form::text('postcode', $company->postcode, array('placeholder' => '', 'class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-content">
              <button type="button" class="btn btn-primary" onclick="getByPostCode()">Search</button>
            </div>
          </div>

          <div id="form-display-mail" class="form-group ">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
              <span class="label label-danger label-required">Required</span>
              <strong class="field-title">Prefecture</strong>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
              <select name="prefecture" id="prefecture" class="form-control">
                <option value="">Select Prefecture</option>
                @foreach ($prefecture as $item)
                <option value="{{$item->display_name}}">{{ $item->display_name }}</option>
                @endforeach
              </select>
            </div>

          </div>

          <div id="form-display-mail" class="form-group ">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
              <span class="label label-danger label-required">Required</span>
              <strong class="field-title">City</strong>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
              {{ Form::text('city', $company->display_name, array('placeholder' => '', 'class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
            </div>

          </div>

          <div id="form-display-mail" class="form-group ">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
              <span class="label label-danger label-required">Required</span>
              <strong class="field-title">Local</strong>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
              {{ Form::text('local', $company->display_name, array('placeholder' => '', 'class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
            </div>
          </div>

          <div id="form-display-mail" class="form-group ">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
              <strong class="field-title">Street Address</strong>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
              {{ Form::text('street_address', $company->street_address, array('placeholder' => '', 'class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
            </div>
          </div>

          <div id="form-display-mail" class="form-group ">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
              <strong class="field-title">Bussiness Hour</strong>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
              {{ Form::text('business_hour', $company->business_hour, array('placeholder' => '', 'class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
            </div>
          </div>
          <div id="form-display-mail" class="form-group ">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
              <strong class="field-title">Regular Holiday </strong>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
              {{ Form::text('regular_holiday', $company->regular_holiday, array('placeholder' => '', 'class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
            </div>
          </div>

          <div id="form-display-mail" class="form-group ">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
              <strong class="field-title">Phone</strong>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
              {{ Form::text('phone', $company->phone, array('placeholder' => '', 'class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
            </div>
          </div>

          <div id="form-display-mail" class="form-group ">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
              <strong class="field-title">Fax</strong>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
              {{ Form::text('fax', $company->fax, array('placeholder' => '', 'class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
            </div>
          </div>

          <div id="form-display-mail" class="form-group ">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
              <strong class="field-title">URL</strong>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
              {{ Form::text('url', $company->url, array('placeholder' => '', 'clavaluess' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
            </div>
          </div>

          <div id="form-display-mail" class="form-group ">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
              <strong class="field-title">License Number</strong>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
              {{ Form::text('license_number', $company->license_number, array('placeholder' => '', 'class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
            </div>
          </div>

          <div id="form-display-mail" class="form-group ">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
              <span class="label label-danger label-required">Required</span>
              <strong class="field-title">Image</strong>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
              <input type="file" class="form-control" id="image" name="image">
              @if($company->page_type == 'create')
              <img src="#" id="preview-image-before-upload" width="200px" />
              @else
              <img src="{{asset('/uploads/files/' .$company->image)}}" id="preview-image-before-upload" width="200px" />
              @endif
              <!--for preview purpose -->
            </div>
          </div>


          <div id="form-button" class="form-group no-border">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center" style="margin-top: 20px;">
              <button type="submit" name="submit" id="send" class="btn btn-primary">Submit</button>
            </div>
          </div>
          {{ Form::close() }}
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@endsection

@section('title', 'Company | ' . env('APP_NAME',''))

@section('body-class', 'custom-select')

@section('css-scripts')
@endsection

@section('js-scripts')
<script src="{{ asset('bower_components/bootstrap/js/tooltip.js') }}"></script>
<!-- validationEngine -->
<script src="{{ asset('js/3rdparty/validation-engine/jquery.validationEngine-en.js') }}"></script>
<script src="{{ asset('js/3rdparty/validation-engine/jquery.validationEngine.js') }}"></script>
<script src="{{ asset('js/backend/companies/form.js') }}"></script>
<script>
  $('#image').change(function() {

    let reader = new FileReader();

    reader.onload = (e) => {

      $('#preview-image-before-upload').attr('src', e.target.result);
    }

    reader.readAsDataURL(this.files[0]);

  });



  function getByPostCode() {
    var postcode = document.getElementsByName('postcode')[0].value;

    $.get("/companies/postcode/" + postcode, function(data) {
      document.getElementsByName('city')[0].value = data.city;
      document.getElementsByName('local')[0].value = data.local;
      var prefecture = document.getElementsByName('prefecture')[0].value = data.prefecture;
      opt = document.createElement("option");
      opt.value = data.prefecture;
      opt.textContent = data.prefecture;

    });
  }
</script>
@endsection