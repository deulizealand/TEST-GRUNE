<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Company;

class ApiCompaniesController  extends Controller
{

  /**
   * Return the contents of User table in tabular form
   *
   */
  public function getCompaniesTabular()
  {
    $company = Company::leftJoin('prefectures', 'companies.prefecture_id', '=', 'prefectures.id')->orderBy('companies.id', 'desc')->get(['display_name', 'companies.name', 'companies.email', 'postcode', 'street_address', 'updated_at', 'companies.id']);
    return response()->json($company);
  }
}
