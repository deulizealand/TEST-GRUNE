<?php

namespace App\Http\Controllers\Backend;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Postcode;
use App\Models\Prefecture;
use Config;
use DB;
use File;

class CompanyController extends Controller
{

  /**
   * Get named route
   *
   */
  private function getRoute()
  {
    return 'admin.companies';
  }

  /**
   * Validator for user
   *
   * @return \Illuminate\Http\Response
   */
  protected function validator(array $data, $type)
  {
    // Determine if password validation is required depending on the calling
    return Validator::make($data, [
      'name' => 'required',
      'email' => 'required|email',
      'postcode' => 'required',
      'prefecture' => 'required',
      'city' => 'required',
      'local' => 'required',
      'image' => 'required',
      // (update: not required, create: required)
    ]);
  }

  public function index()
  {
    return view('backend.companies.index');
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function add()
  {
    $prefecture = Prefecture::get();
    $company = new Company();
    $company->form_action = $this->getRoute() . '.create';
    $company->page_title = 'Company Add Page';
    $company->page_type = 'create';
    return view('backend.companies.form', [
      'prefecture' => $prefecture,
      'company' => $company,
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function create(Request $request)
  {
    $newCompany = $request->all();

    // Validate input, indicate this is 'create' function
    $this->validator($newCompany, 'create')->validate();

    try {

      $file = $request->file('image');
      $fileName = $file->getClientOriginalName();
      $destinationPath = public_path() . '/uploads/files';
      $file->move($destinationPath, $fileName);
      $newCompany['image'] = $fileName;
      $prefectuireId = Prefecture::where('display_name', $newCompany['prefecture'])->first();
      $newCompany['prefecture_id'] = $prefectuireId['id'];
      $company = Company::create($newCompany);
      if ($company) {

        // we can use latest data first to get the ID before it stored for name of image but i think is so danger if more than 1 user do the task.
        $companyId = $company->id;
        $update = Company::find($companyId);
        $update->image = 'image_' . $companyId . '.' . $file->getClientOriginalExtension();
        $update->save();
        // Create is successful, back to list
        return redirect()->route($this->getRoute())->with('success', Config::get('const.SUCCESS_CREATE_MESSAGE'));
      } else {
        // Create is failed
        return redirect()->route($this->getRoute())->with('error', Config::get('const.FAILED_CREATE_MESSAGE'));
      }
    } catch (Exception $e) {
      // Create is failed
      return redirect()->route($this->getRoute())->with('error', Config::get('const.FAILED_CREATE_MESSAGE'));
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $prefecture = Prefecture::get();
    $company = company::find($id);
    $company->form_action = $this->getRoute() . '.update';
    $company->page_title = 'Company Edit Page';
    // Add page type here to indicate that the form.blade.php is in 'edit' mode
    $company->page_type = 'edit';
    return view('backend.companies.form', [
      'company' => $company,
      'prefecture' => $prefecture
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
    $newCompany = $request->all();
    // dd($newCompany);
    try {
      $id = $request->get('id');
      $currentCompany = Company::find($id);
      if ($currentCompany) {

        if ($request->hasFile('image')) {
          $file = $request->file('image');
          $fileName = 'image_' . $id . '.' . $file->getClientOriginalExtension();

          $destinationPath = public_path() . '/uploads/files';
          $fileExist = $destinationPath . '/' . $fileName;
          if (File::exists($fileExist)) {
            File::delete($fileExist);
          }
          $file->move($destinationPath, $fileName);
          $newCompany['image'] = $fileName;
        } else {
          $newCompany['image'] = $currentCompany['image'];
        }
        $prefectuireId = Prefecture::where('display_name', $newCompany['prefecture'])->first();
        $newCompany['prefecture_id'] = $prefectuireId['id'];
        // Validate input only after getting password, because if not validator will keep complaining that password does not meet validation rules
        // Hashed password from DB will always have length of 60 characters so it will pass validation
        // Also indicate this is 'update' function
        $this->validator($newCompany, 'update')->validate();

        // Update user
        $currentCompany->update($newCompany);

        // If update is successful
        return redirect()->route($this->getRoute())->with('success', Config::get('const.SUCCESS_UPDATE_MESSAGE'));
      } else {
        // If update is failed
        return redirect()->route($this->getRoute())->with('error', Config::get('const.FAILED_UPDATE_MESSAGE'));
      }
    } catch (Exception $e) {
      // If update is failed
      return redirect()->route($this->getRoute())->with('error', Config::get('const.FAILED_UPDATE_MESSAGE'));
    }
  }

  public function delete($id)
  {
    try {
      // Get user by id
      $company = Company::find($id);
      // If to-delete user is not the one currently logged in, proceed with delete attempt

      // Delete user
      $company->delete();

      // If delete is successful
      return redirect()->route($this->getRoute())->with('success', Config::get('const.SUCCESS_DELETE_MESSAGE'));
    } catch (Exception $e) {
      // If delete is failed
      return redirect()->route($this->getRoute())->with('error', Config::get('const.FAILED_DELETE_MESSAGE'));
    }
  }


  public function postcode($id)
  {
    $postCodeData = Postcode::where('postcode', $id)->first();

    return response()->json($postCodeData);
  }
}
